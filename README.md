![Logo robado](https://cdn.discordapp.com/attachments/787363155494830091/895758586385424444/unknown.png)

# Proyecto ApiRest + DTO

El proyecto consiste en una api capaz de controlar una **aplicación** de *música*,

incluyendo sus *artistas* y por supuesto *listas de canciones* para poder hacer tus propias configuraciones.

### Los desarrolladores son:
- Cynthia Labrador
- Alejandro Bajo
- Manuel Fernandez
- Pablo Repiso


## Prerequisitos  📋

Para esta aplicación necesitarás el software de  [intelilliJ IDEA](https://www.jetbrains.com/es-es/idea/)

También para comprobar las peticiones, te recomendamos que uses PostMan el cual te facilitaremos un área de

trabajo exportada en un archivo llamado **P01-Grupo 6.postman_collection** que lo encontrás dentro del

repositorio que está aquí: [Repositorio]( https://PabloRepiso@bitbucket.org/cynthialabrador/proyecto-p01-grupo6.git)

![logo](https://resources.jetbrains.com/storage/products/intellij-idea/img/meta/intellij-idea_logo_300x300.png)

## Ejecutando las pruebas ⚙

Este programa se encarga de registrar artistas, canciones y generar las playList con las canciones registradas.

Que tú mismo puedes subir.

Tendrás acceso a las listas con su contenido, podrás conocer todos los datos de los artistas y las canciones,

incluso tendrás la opción de borrar aquellos artistas, playList o canciones que no te gustan y como no,

podrás actualizar todos los datos de tus artistas favoritos y sus éxitos.

A continuación te dejamos un enlace para que veas de manera más detallada todas las opciones y funcionalidades

comentas para que puedas comprenderlo mejor y de manera más visual.


### Métodos del programa 

 [Swagger]( [enlace](http://localhost:8080/swagger-ui/index.html)) 

### Ejecutando pruebas 🛠

[Spring init](https://start.spring.io/)

### IntelillJ IDEA

### Maven - manejador de dependencias

### Expresiones de gratitud

Un **gin tonic** a tu salud Luismi  ❤️ 🍸



