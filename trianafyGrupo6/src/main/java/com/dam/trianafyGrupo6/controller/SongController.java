package com.dam.trianafyGrupo6.controller;

import com.dam.trianafyGrupo6.dto.SongDto;
import com.dam.trianafyGrupo6.dto.SongDtoConverter;
import com.dam.trianafyGrupo6.dto.SongUpdateDto;
import com.dam.trianafyGrupo6.dto.SongUpdateDtoConverter;
import com.dam.trianafyGrupo6.model.Artist;
import com.dam.trianafyGrupo6.model.Song;
import com.dam.trianafyGrupo6.repository.ArtistRepository;
import com.dam.trianafyGrupo6.repository.SongRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.parser.Entity;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/songs")
@Tag(name = "Song", description = "Clase controlador de la entidad Song")
public class SongController {

    private final SongRepository songRepository;
    private final SongDtoConverter songDtoConverter;
    private final SongUpdateDtoConverter songUpdateDtoConverter;
    private final ArtistRepository artistRepository;


    @Operation(summary = "Crea una nueva canción")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "400",
                    description = "Faltan uno o más de los atributos de la canción",
                    content = @Content),
            @ApiResponse(responseCode = "201",
                    description = "Se ha creado la nueva canción correctamente",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = Song.class))
                    })
    })
    @PostMapping("/")
    public ResponseEntity<Song> createSong(@RequestBody SongDto dtoSong) {
        if (dtoSong.getAlbum()==null || dtoSong.getTitle()==null || dtoSong.getYear()==null || dtoSong.getArtistName()==null) {
            return ResponseEntity.badRequest().build();
        }

        Song nueva = songDtoConverter.songDtoToSong(dtoSong);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(songRepository.save(nueva));
    }

    @Operation(summary = "Encuentra todas las canciones exsistentes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404",
                    description = "No se han encontrado canciones",
                    content = @Content),
            @ApiResponse(responseCode = "200",
                    description = "Aquí están todas las canciones",
                    content = {
                            @Content(mediaType = "aplication/json",
                                    schema = @Schema(implementation = Song.class))
                    })
    })
    @GetMapping("")
    public ResponseEntity<List<SongDto>> findAll() {

        List<Song> canciones = songRepository.findAll();
        if (canciones.isEmpty()) {
            return ResponseEntity.notFound().build();
        } else {
            List<SongDto> lista =
                    canciones.stream()
                            .map(songDtoConverter::songToSongDto)
                            .collect(Collectors.toList());
            return ResponseEntity
                    .ok()
                    .body(lista);
        }
    }

    @Operation(summary = "Muestra la información de una cancion buscando por id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404",
                    description = "No se ha encontrado ninguna canción con ese id",
                    content = @Content),
            @ApiResponse(responseCode = "200",
                    description = "Aquí tiene su canción",
                    content = {
                            @Content(mediaType = "aplication/json",
                                    schema = @Schema(implementation = Song.class))
                    })
    })
    @GetMapping("/{id}")
    public ResponseEntity<Song> findOne(@PathVariable Long id) {
        return ResponseEntity
                .of(songRepository.findById(id));
    }

    @Operation(summary = "Elimina una canción por su id")
    @ApiResponse(responseCode = "204",
            description = "Canción eliminada",
            content = @Content)
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        if (songRepository.existsById(id)) {
            songRepository.deleteById(id);
        }
        return ResponseEntity.noContent().build();
    }





    @Operation(summary = "Actualiza una canción")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "400",
                    description = "Faltan uno o más de los atributos de la canción",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "No se encuantra el archivo a modificar",
                    content = @Content),
            @ApiResponse(responseCode = "201",
                    description = "Se ha actualizado una canción correctamente",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = Song.class)),

                    })
    })
    @PutMapping ("/{id}")
    public ResponseEntity<SongUpdateDto> songUpdate (@PathVariable ("id") Long id, @RequestBody SongUpdateDto dto)
    {

        if (dto.getAlbum()==null|| dto.getTitle()==null||dto.getYear()==null||dto.getArtistName()==null)
            return  ResponseEntity.badRequest().build();

            return ResponseEntity.of(songRepository.findById(id).map(s->{s.setAlbum(dto.getAlbum());
            s.setTitle(dto.getTitle());
            s.setArtist(Artist.builder().name(artistRepository.findById(songRepository.findById(id).get().getArtist().getId()).get().getName()).build());
            s.setYear(dto.getYear());
            songRepository.save(s);
            return songUpdateDtoConverter.songToSongUpdateDto(s,id,dto.getArtistName());}
            ));

    }

}