package com.dam.trianafyGrupo6.controller;

import com.dam.trianafyGrupo6.dto.CreatePlaylistDto;
import com.dam.trianafyGrupo6.dto.PlaylistDto;
import com.dam.trianafyGrupo6.dto.PlaylistDtoConverter;
import com.dam.trianafyGrupo6.dto.PlaylistParamDto;
import com.dam.trianafyGrupo6.model.Playlist;
import com.dam.trianafyGrupo6.model.Song;
import com.dam.trianafyGrupo6.repository.PlaylistRepository;
import com.dam.trianafyGrupo6.repository.SongRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor

@Tag(name = "Playlist",description = "El contolador que se encarga de las PlayList")
public class PlaylistController {

    private final PlaylistRepository playlistRepository;
    private final SongRepository songRepository;
    private final PlaylistDtoConverter dtoConverter;

    @Operation(summary = "Devuelve la playList")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404",
                    description = "No se ha encontrado la playList",
                    content = @Content),
            @ApiResponse(responseCode = "200",
                    description = "PlayList encontrada y servida",
                    content = {
                            @Content (mediaType = "application/json",
                                    schema = @Schema(implementation = Playlist.class))
                    })
    })

    @GetMapping ({"list/{id}","/lists/{id}/songs"})
    public  ResponseEntity <Playlist> informationPlayList (@PathVariable long id)
    {
        return ResponseEntity.of(playlistRepository.findById(id));
    }

    @Operation(summary = "El controlador te muestra la lista de Playlists que existen")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404",
                    description = "No se han encontrado playLists",
                    content = @Content),
            @ApiResponse(responseCode = "200",
                    description = "Te lista las playlists",
                    content = {
                            @Content (mediaType = "aplication/json",
                                    schema = @Schema(implementation = Playlist.class))
                    })
    })
    @GetMapping("/lists/")
    public ResponseEntity<List<PlaylistDto>> findAll() {

        return (playlistRepository.findAll().isEmpty())?ResponseEntity.notFound().build():ResponseEntity.ok().body(playlistRepository
                .findAll().stream().map(dtoConverter::playlistToPlaylistDto).collect(Collectors.toList()));
    }


    @Operation(summary = "Agrega una canción a un playList")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404",
                    description = "No se ha encontrado la playList o la canción",
                    content = @Content),
            @ApiResponse(responseCode = "201",
                    description = "Se ha añadido la canción",
                    content = {
                            @Content (mediaType = "aplication/json",
                                    schema = @Schema(implementation = Playlist.class))
                    })
    })
    @GetMapping("/list/{id1}/songs/{id2}")
    public ResponseEntity<Song> findASong(@PathVariable ("id1") Long idPlaylist, @PathVariable("id2") Long idSong){
            List<Song> lista = playlistRepository.findById(idPlaylist).stream().map(p->p.getSongs()).collect(Collectors.toList()).get(0);

        return ResponseEntity.of(lista.stream().filter(s->s.getId()==idSong).findFirst());

    }

    @Operation(summary = "Crea una nueva Playlist")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "400",
                    description = "Datos introducidos invalidos",
                    content = @Content),
            @ApiResponse(responseCode = "201",
                    description = "Playlist creada",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = PlaylistParamDto.class))
                    })
    })

    @PostMapping("/lists/")
    public ResponseEntity<PlaylistParamDto> create(
            @RequestBody CreatePlaylistDto dto) {

        if (dto.getName() == null || dto.getDescription() == null) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(dtoConverter.playlistToReturnPlaylistDto(playlistRepository.save(dtoConverter.createPlaylistDtoToPlaylist(dto))));

    }

    @Operation(summary = "Agrega una canción a un playList")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404",
                    description = "No se ha encontrado la playList o la canción",
                    content = @Content),
            @ApiResponse(responseCode = "201",
                    description = "Se ha añadido la canción",
                    content = {
                            @Content (mediaType = "aplication/json",
                                    schema = @Schema(implementation = Playlist.class))
                    })
    })

        @PostMapping ("/lists/{id1}/songs/{id2}")
        public ResponseEntity<Playlist> addSongToPlaylist (@PathVariable("id1") Long idPlaylist, @PathVariable ("id2") Long idSong)
    {
                Optional<Playlist> playlistTemp=playlistRepository.findById(idPlaylist);
                Optional<Song> songTemp=songRepository.findById(idSong);

                if(playlistTemp.isEmpty()||songTemp.isEmpty())
                        return ResponseEntity.notFound().build();
               playlistTemp.get().getSongs().add(songTemp.get());
               return ResponseEntity.status(HttpStatus.OK).body(playlistRepository.save(playlistTemp.get()));
            }



    @Operation(summary = "Actualiza una playlist")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404",
                    description = "No se ha encontrado la playList",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Datos erroneos",
                    content = @Content),
            @ApiResponse(responseCode = "200",
                    description = "Se ha actualizado la canción",
                    content = {
                    @Content (mediaType = "aplication/json",
                            schema = @Schema(implementation = Playlist.class))
            })
    })
    @PutMapping("/list/{id}")
    public ResponseEntity<PlaylistParamDto> updatePlaylist (@RequestBody PlaylistParamDto dto, @PathVariable ("id") Long id)
    {
        if(dto.getName() == null || dto.getDescription() == null)
        return ResponseEntity.badRequest().build();
        return ResponseEntity.of(playlistRepository.findById(id).map(p-> {p.setName(dto.getName());
                p.setDescription(dto.getDescription());
                playlistRepository.save(p);
                return dtoConverter.updatePlaylistToPlaylistDto(p);
    }));
    }




    @Operation(summary = "Elimina una playlist por su id")
    @ApiResponse(responseCode = "204",
            description = "La playlist se ha eliminado correctamente",
            content = @Content)
    @DeleteMapping("list/{id}")
    public ResponseEntity<?> deletePlaylist (@PathVariable Long id){
        if (playlistRepository.existsById(id)){
            playlistRepository.deleteById(id);
        }
        return ResponseEntity.noContent().build();
    }





    @Operation(summary = "Actualiza una playlist")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404",
                    description = "No se ha encontrado la playList",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Datos erroneos",
                    content = @Content),
            @ApiResponse(responseCode = "200",
                    description = "Se ha actualizado la playList",
                    content = {
                            @Content (mediaType = "aplication/json",
                                    schema = @Schema(implementation = Playlist.class))
                    })
    })

    @DeleteMapping("/list/{id1}/songs/{id2}")
    public ResponseEntity<?> deleteSongFromPlaylist(@PathVariable("id1")Long idPlaylist, @PathVariable("id2") Long idSong){
        Optional<Playlist> pl = playlistRepository.findById(idPlaylist);
        Optional<Song> sg= songRepository.findById(idSong);
        pl.get().getSongs().remove(sg.get());
        playlistRepository.save(pl.get());

        if (pl.isEmpty())
            return ResponseEntity.noContent().build();
        return ResponseEntity.noContent().build();

    }
}
