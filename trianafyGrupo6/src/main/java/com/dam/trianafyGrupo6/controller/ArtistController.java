package com.dam.trianafyGrupo6.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.dam.trianafyGrupo6.model.Artist;
import com.dam.trianafyGrupo6.repository.ArtistRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import java.util.Optional;
import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/artist")
@Tag(name = "Artist",description = "El contolador que se encarga de los artistas")
public class ArtistController {

    private final ArtistRepository artistRepository;

    @Operation(summary = "Obtiene un artista por su id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404",
            description = "Artista no encontrado",
            content = @Content),
            @ApiResponse(responseCode = "200",
            description = "Artista encontrado",
            content = {
                    @Content(mediaType = "application/json",
                    schema = @Schema(implementation = Artist.class))
            })
    })
    @GetMapping("/{id}")
    public ResponseEntity<Artist> findOne(
            @Parameter(description = "Id del artista a encontrar")
            @PathVariable Long id
    ) {

        Optional<Artist> artist = artistRepository.findById(id);

        return ResponseEntity.of(artist);

    }

    @Operation(summary = "Devuelve la lista de todos los artistas")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404",
            description = "No se han encontrado los artistas",
            content = @Content),
            @ApiResponse(responseCode = "200",
            description = "Se han encontrado los artistas",
            content = {
                    @Content(mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = Artist.class)))
            })
    })
    @GetMapping("/")
    public ResponseEntity<List<Artist>> findAll() {

        List<Artist> data = artistRepository.findAll();

        return (data.isEmpty())?ResponseEntity.notFound().build():ResponseEntity.ok().body(data);

    }

    @Operation(summary = "Crea un nuevo artista en la base de datos")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "400",
            description = "Se han introducido mal los datos",
            content = @Content),
            @ApiResponse(responseCode = "201",
            description = "Creado nuevo artista en la base de datos",
            content = {
                    @Content(mediaType = "application/json",
                    schema = @Schema(implementation = Artist.class))
            })
    })
    @PostMapping("/")
    public ResponseEntity<Artist> create(@RequestBody Artist artist) {

        if (artist.getName().isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(artistRepository.save(artist));

    }

    @Operation(summary = "Cambia los datos de un artista por su id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404",
            description = "Artista no encontrado",
            content = @Content),
            @ApiResponse(responseCode = "200",
            description = "Artista modificado con exito",
            content = {
                    @Content(mediaType = "applicatio/json",
                    schema = @Schema(implementation = Artist.class))
            })
    })
    @PutMapping("/{id}")
    public ResponseEntity<Artist> edit(
            @Parameter(description = ("Id del artista"))
            @PathVariable Long id,
            @RequestBody Artist cambio){


        return ResponseEntity.of(
                artistRepository.findById(id).map(artist -> {
                    artist.setName(cambio.getName());
                    artistRepository.save(artist);
                    return artist;
                }));

    }

    @Operation(summary = "Elimina un artista por su id")
    @ApiResponse(responseCode = "204",
            description = "Artista eliminado",
            content = @Content)
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(
            @Parameter(description = "Id del artista")
            @PathVariable Long id) {
        if (artistRepository.existsById(id)) {
            artistRepository.deleteById(id);
        }
        return ResponseEntity.noContent().build();
    }
}
