package com.dam.trianafyGrupo6.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
@Builder
@Entity
@Data @AllArgsConstructor @NoArgsConstructor
public class Song {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    @ManyToOne (cascade = CascadeType.PERSIST)
    private Artist artist;

    private String album;
    private String year;

    public Song(String title, Artist artist, String album, String year) {
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.year = year;
    }

    public Song(String title, String album, String year) {
        this.title = title;
        this.album = album;
        this.year = year;
    }


}
