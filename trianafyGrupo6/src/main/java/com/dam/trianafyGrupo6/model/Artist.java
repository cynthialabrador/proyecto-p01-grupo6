package com.dam.trianafyGrupo6.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Builder
@Entity
@Data @AllArgsConstructor @NoArgsConstructor
public class Artist {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    public Artist(String name) {
        this.name = name;
    }
}
