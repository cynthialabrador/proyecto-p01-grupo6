package com.dam.trianafyGrupo6.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor @AllArgsConstructor
public class Playlist {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @Lob
    private String description;

    @ElementCollection
    private List<Song> songs;

    public Playlist(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Playlist(String name) {
        this.name = name;
    }
}
