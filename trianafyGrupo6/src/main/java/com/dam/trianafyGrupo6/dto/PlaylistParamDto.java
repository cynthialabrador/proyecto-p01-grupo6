package com.dam.trianafyGrupo6.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Lob;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PlaylistParamDto {
    public Long id;
    public String name;

    @Lob
    public String description;

}
