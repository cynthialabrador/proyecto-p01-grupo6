package com.dam.trianafyGrupo6.dto;

import com.dam.trianafyGrupo6.model.Artist;
import com.dam.trianafyGrupo6.model.Song;
import org.springframework.stereotype.Controller;

@Controller
public class SongUpdateDtoConverter {


    public SongUpdateDto songToSongUpdateDto (Song song, Long id, String nombre){
        return SongUpdateDto.builder().
                title(song.getTitle()).
                artistName(nombre).
                album(song.getAlbum()).
                year(song.getYear()).id(id)
                .build();
    }
}
