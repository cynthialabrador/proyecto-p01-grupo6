package com.dam.trianafyGrupo6.dto;

import com.dam.trianafyGrupo6.model.Playlist;
import org.springframework.stereotype.Component;

@Component
public class PlaylistDtoConverter {
    public Playlist playlistDtoToPlaylist (PlaylistDto p){
        return new Playlist(
                p.getName()
        );
    }
    public PlaylistDto playlistToPlaylistDto(Playlist p){
        PlaylistDto play = new PlaylistDto();
        play.setId(p.getId());
        play.setName(p.getName());
        play.setTotalSongs((p.getSongs().stream().count()));
        return play;
    }

    public PlaylistParamDto updatePlaylistToPlaylistDto(Playlist p){
        PlaylistParamDto play = new PlaylistParamDto();
        play.setId(p.getId());
        play.setName(p.getName());
        play.setDescription(p.getDescription());
        return play;
    }

    public Playlist createPlaylistDtoToPlaylist(CreatePlaylistDto dto) {

        return new Playlist(
                dto.getName(),
                dto.getDescription()
        );

    }
    public PlaylistParamDto playlistToReturnPlaylistDto(Playlist playlist) {

        return new PlaylistParamDto(
                playlist.getId(),
                playlist.getName(),
                playlist.getDescription()
        );

    }
}
