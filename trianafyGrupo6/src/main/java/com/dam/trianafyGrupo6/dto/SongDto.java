package com.dam.trianafyGrupo6.dto;

import com.dam.trianafyGrupo6.model.Artist;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SongDto {
    private String title;
    private String artistName;
    private String album;
    private String year;

}
