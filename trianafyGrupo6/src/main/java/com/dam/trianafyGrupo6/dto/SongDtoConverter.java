package com.dam.trianafyGrupo6.dto;

import com.dam.trianafyGrupo6.model.Artist;
import com.dam.trianafyGrupo6.model.Song;
import org.springframework.stereotype.Component;

@Component
public class SongDtoConverter {

    public Song songDtoToSong(SongDto song) {

        Artist artist=new Artist(song.getArtistName());
        return new Song(
                song.getTitle(),
                artist,
                song.getAlbum(),
                song.getYear()
        );
    }

    public SongDto songToSongDto(Song song) {
        SongDto nueva = new SongDto();
        nueva.setTitle(song.getTitle());
        nueva.setArtistName(song.getArtist().getName());
        nueva.setAlbum(song.getAlbum());
        nueva.setYear(song.getYear());
        return nueva;
    }
}
