package com.dam.trianafyGrupo6.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Lob;


@Data
@NoArgsConstructor @AllArgsConstructor
public class CreatePlaylistDto {

    private String name;
    @Lob
    private String description;

}