package com.dam.trianafyGrupo6.repository;

import com.dam.trianafyGrupo6.model.Artist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArtistRepository extends JpaRepository<Artist, Long> {
}
