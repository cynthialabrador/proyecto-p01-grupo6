package com.dam.trianafyGrupo6.repository;

import com.dam.trianafyGrupo6.model.Song;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SongRepository extends JpaRepository<Song, Long> {
}
