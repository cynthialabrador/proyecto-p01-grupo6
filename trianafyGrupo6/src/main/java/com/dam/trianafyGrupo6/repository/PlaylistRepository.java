package com.dam.trianafyGrupo6.repository;

import com.dam.trianafyGrupo6.model.Playlist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaylistRepository extends JpaRepository <Playlist, Long> {
}
