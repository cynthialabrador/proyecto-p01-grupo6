package com.dam.trianafyGrupo6;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info =
@Info(description = "Proyecto P01 Grupo 6",
		contact = @Contact(name = "Grupo 6"),
		title = "API Listas de Reprodución de música"
)
)
public class TrianafyGrupo6Application {

	public static void main(String[] args) {
		SpringApplication.run(TrianafyGrupo6Application.class, args);
	}

}
